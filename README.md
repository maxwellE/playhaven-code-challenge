PlayHaven Coding Challenge
==============================
By: Maxwell Elliott, <elliott.432@osu.edu>

Assumptions
----------------------
1.    You have [redis](http://redis.io) installed and it is running

*    If you are on a Mac and have [homebrew](http://mxcl.github.com/homebrew/) it is as easy as:

        $ brew install redis

*    To see if redis is active try running the following command:

        $ ps aux | grep redis

 If something comes up other than 'grep redis' then it should be running.

Getting Started
-----------------------
1.    Install requirements 

        $ pip install -r requirements.txt

2.    Run server
          
        $ ./maxwell-elliott-playhaven.py
 
And that is it! The server is now listening on [localhost on port 8080](http://127.0.0.1:8080).

*    To kill the server simply press "Ctrl-C".

Libraries Used
--------------------
*    BeautifulSoup
*    gevent
*    greenlet
*    numpy
*    redis
*    wsgiref

Using Python version 2.7.3
