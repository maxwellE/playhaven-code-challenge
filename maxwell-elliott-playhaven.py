#! /usr/bin/env python
from gevent import pywsgi
import gevent.event 
import signal
import sys
import re
import json
import httplib
import hashlib
import redis
from BeautifulSoup import BeautifulSoup

ev_stop = gevent.event.Event() 

def signal_handler(_=None, __=None):
        print ""
        ev_stop.set() 

def responder(environ, start_response):
    fib_match = re.match('^/fib/(\d+)$',environ['PATH_INFO'])
    if fib_match:
        val = int(fib_match.group(1))
        start_response('200 OK', [('Content-Type', 'json')])
        yield json.dumps({"response":fib(val)})
    elif environ['PATH_INFO'] == "/google-body":
        google_con = httplib.HTTPConnection('www.google.com')
        google_con.request('GET','')
        soup = BeautifulSoup(google_con.getresponse())
        start_response('200 OK', [('Content-Type', 'json')])
        yield json.dumps({"response":hashlib.sha1(soup.find('body').__str__()).hexdigest()})
    elif environ['PATH_INFO'] == "/store":
        r = redis.StrictRedis(host='localhost', port=6379, db=0)
        param_str = environ['wsgi.input'].read()
        if environ['REQUEST_METHOD'] == "POST":
            if re.match('^value=.+$',param_str):
                store_match = re.match('^value=(.+)$',param_str)
                r.set('value',store_match.group(1))
                start_response('200 OK', [('Content-Type', 'json')])
            else:
                start_response('400 Bad Request', [('Content-Type', 'json')])
                yield json.dumps({"response":"Invalid parameter provided. Expected a string with the format of 'value=<STRING>'"})
        elif environ['REQUEST_METHOD'] == "GET":
            start_response('200 OK', [('Content-Type', 'json')])
            yield json.dumps({"response": r.get('value')})
    else:
        start_response('404 Not Found', [('Content-Type', 'json')])
        yield json.dumps({"response": "404 Not Found"})

def fib(n):
    if n == 0:
        return 0
    elif n == 1:
        return 1
    else:
        return fib(n-1) + fib(n-2)

def main():
    server = pywsgi.WSGIServer(
        ('', 8080), responder)
    print "Starting server at http://127.0.0.1:8080"
    server.serve_forever()

if __name__ == '__main__':
    try:
        gevent.signal(signal.SIGINT, signal_handler)
        signal.signal(signal.SIGINT, signal_handler)
        main()
    except KeyboardInterrupt:
        print "Saw SIGINT, shutting server down..."
        sys.exit(0)
